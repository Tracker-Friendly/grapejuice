msgid ""
msgstr ""
"Project-Id-Version: grapejuice\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-20 19:38+0000\n"
"PO-Revision-Date: 2022-12-06 15:01+0000\n"
"Last-Translator: \n"
"Language-Team: Spanish - Spain\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/grapejuice/cli/main.py:146
msgid "Are you sure you want to uninstall Grapejuice? [y/N] "
msgstr "¿Está seguro de que desea desinstalar Grapejuice? [s/N] "

#: src/grapejuice/cli/main.py:151
msgid ""
"Remove the Wineprefixes that contain your installations of Roblox? This will "
"cause all configurations for Roblox to be permanently deleted! [n/Y] "
msgstr ""
"¿Eliminar los Wineprefixes con instalaciones de Roblox? ¡Esto eliminará la "
"configuración de Roblox para siempre! [n/S] "

#: src/grapejuice/cli/main.py:159
msgid "Grapejuice has been uninstalled. Have a nice day!"
msgstr "Grapejuice ha sido desinstalado."

#: src/grapejuice/cli/main.py:162
msgid "Uninstallation aborted"
msgstr "Desinstalación cancelada"

#: src/grapejuice/components/main_window_components.py:51
msgid "Start"
msgstr "Inicio"

#: src/grapejuice/gui_task_manager.py:12
msgid "This task is already being performed!"
msgstr "¡Esta tarea ya se está realizando!"

#: src/grapejuice/helpers/background_task_helper.py:95
#, python-brace-format
msgid "Grapejuice is running {count} background tasks"
msgstr "Grapejuice está ejecutando {count} trabajos en el fondo"

#: src/grapejuice/helpers/prefix_feature_toggles.py:24
msgid "Desktop App"
msgstr "App"

#: src/grapejuice/helpers/prefix_feature_toggles.py:28
msgid "Studio"
msgstr "Studio"

#: src/grapejuice/helpers/prefix_feature_toggles.py:32
msgid "Experience Player"
msgstr "Player"

#: src/grapejuice/helpers/prefix_feature_toggles.py:47
msgid "Application Hints"
msgstr "Para que usar esto"

#: src/grapejuice/helpers/prefix_feature_toggles.py:48
msgid ""
"Grapejuice uses application hints to determine which prefix should be used "
"to launch a Roblox application. If you toggle the hint for a Roblox "
"application on for this prefix, Grapejuice will use this prefix for that "
"application."
msgstr ""
"Grapejuice usa esto para determinar qué prefix se debe usar para iniciar "
"una aplicación de Roblox. Grapejuice utilizará este prefix para las aplicaciónes."

#: src/grapejuice/helpers/prefix_feature_toggles.py:64
msgid "Roblox Renderer"
msgstr "API de gráficos"

#: src/grapejuice/helpers/prefix_feature_toggles.py:90
msgid "Use PRIME offloading"
msgstr "Usar PRIME"

#: src/grapejuice/helpers/prefix_feature_toggles.py:95
msgid "PRIME offload sink"
msgstr "PRIME offload sink"

#: src/grapejuice/helpers/prefix_feature_toggles.py:105
msgid "Use Mesa OpenGL version override"
msgstr "Anular la versión de OpenGL de Mesa"

#: src/grapejuice/helpers/prefix_feature_toggles.py:122
msgid "Graphics Settings"
msgstr "Configuración de gráficos"

#: src/grapejuice/helpers/prefix_feature_toggles.py:123
msgid ""
"Grapejuice can assist with graphics performance in Roblox. These are the "
"settings that control Grapejuice's graphics acceleration features."
msgstr ""
"Grapejuice puede mejorar el rendimiento de gráficos. Esto es la "
"configuración de aceleración de gráficos de Grapejuice."

#: src/grapejuice/helpers/prefix_feature_toggles.py:131
msgid "Wine debugging settings"
msgstr "Configuración de depuración de Wine"

#: src/grapejuice/helpers/prefix_feature_toggles.py:132
msgid ""
"Wine has an array of debugging options that can be used to improve wine. "
"Some of them can cause issues, be careful!"
msgstr ""
"Wine tiene configuración de depuración. Algunas configuraciones pueden "
"causar problemas. ¡Ten cuidado!"

#: src/grapejuice/helpers/prefix_feature_toggles.py:137
msgid "Enable Wine debugging"
msgstr "Usar la depuración de Wine"

#: src/grapejuice/helpers/prefix_feature_toggles.py:142
msgid "WINEDEBUG string"
msgstr "Valor de WINEDEBUG"

#: src/grapejuice/helpers/prefix_feature_toggles.py:151
msgid "Third party application integrations"
msgstr "Aplicaciones de terceros"

#: src/grapejuice/helpers/prefix_feature_toggles.py:152
msgid ""
"Grapejuice can assist in installing third party tools that will improve the "
"Roblox experience"
msgstr "Grapejuice puede instalar aplicaciones de terceros para Roblox"

#: src/grapejuice/helpers/prefix_feature_toggles.py:156
msgid "Use Roblox FPS Unlocker"
msgstr "Usar Roblox FPS Unlocker"

#: src/grapejuice/helpers/prefix_feature_toggles.py:162
msgid "Use DXVK D3D implementation"
msgstr "Usar la implementación DXVK D3D"

#: src/grapejuice/tasks.py:24
msgid "Launching Roblox Studio"
msgstr "Abriendo Roblox Studio"

#: src/grapejuice/tasks.py:37
msgid "Extracting Fast Flags"
msgstr "Extrayendo Fast Flags"

#: src/grapejuice/tasks.py:65
msgid "Opening logs directory"
msgstr "Abriendo el directorio de registros"

#: src/grapejuice/tasks.py:76
msgid "Opening configuration file"
msgstr "Abriendo el archivo de configuración"

#: src/grapejuice/tasks.py:84
msgid "Performing update"
msgstr "Actualizando"

#: src/grapejuice/tasks.py:104
#, python-brace-format
msgid "Installing Roblox in {prefix}"
msgstr "Instalando Roblox en {prefix}"

#: src/grapejuice/tasks.py:115
#, python-brace-format
msgid "Opening Drive C in {prefix}"
msgstr "Abriendo Disco C en {prefix}"

#: src/grapejuice/tasks.py:124
msgid "Opening Studio's sign-in page"
msgstr "Abriendo la página de inicio de sesión de Studio"

#: src/grapejuice/tasks.py:154 src/grapejuice/tasks.py:171
#, python-brace-format
msgid "Running {app} in {prefix}"
msgstr "Ejecutando {app} en {prefix}"

#: src/grapejuice/tasks.py:187
#, python-brace-format
msgid "Killing wineserver for {prefix}"
msgstr "Terminando el wineserver en {prefix}"

#: src/grapejuice/tasks.py:215
#, python-brace-format
msgid "Installing FPS unlocker in {prefix}"
msgstr "Instalando FPS unlocker en {prefix}"

#: src/grapejuice/tasks.py:243
#, python-brace-format
msgid "Updating DXVK state for {prefix}"
msgstr "Actualizando el estado de DXVK en {prefix}"

#: src/grapejuice/tasks.py:263
msgid "Preloading XRandR interface"
msgstr "Precarga de la interfaz XRandR"

#: src/grapejuice/windows/exception_viewer.py:87
msgid "Grapejuice has run into a problem! Please check the details below."
msgstr ""
"¡Grapejuice se ha topado con un problema! Por favor, revisa los detalles "
"debajo."

#: src/grapejuice/windows/exception_viewer.py:88
msgid ""
"Grapejuice has run into multiple problems! Please check the details below."
msgstr ""
"¡Grapejuice se ha topado con multiples problemas! Por favor, revisa los detalles "
"debajo."

#: src/grapejuice/windows/exception_viewer.py:131
msgid "Export error details"
msgstr "Exportar detalles del error"

#: src/grapejuice/windows/exception_viewer.py:164
msgid "File exists"
msgstr "El archivo ya existe"

#: src/grapejuice/windows/exception_viewer.py:165
#, python-brace-format
msgid ""
"The file '{path}' already exists. Are you sure you want to overwrite it?"
msgstr ""
"El archivo «{path}» ya existe, ¿Está seguro de que lo quiere sobrescribir?"

#: src/grapejuice/windows/exception_viewer.py:169
#, python-brace-format
msgid "Canceled write to '{path}' because the file already exists."
msgstr "Escritura a «{path}» cancelada porque el archivo ya existe."

#: src/grapejuice/windows/exception_viewer.py:186
#, python-brace-format
msgid "Successfully saved error information to {path}"
msgstr "Información del error guardada en {path}"

#: src/grapejuice/windows/fast_flag_editor.py:100
msgid "Roblox Player"
msgstr "Roblox Player"

#: src/grapejuice/windows/fast_flag_editor.py:101
msgid "Roblox Studio"
msgstr "Roblox Studio"

#: src/grapejuice/windows/fast_flag_editor.py:102
msgid "Roblox App"
msgstr "Roblox App"

#: src/grapejuice/windows/fast_flag_editor.py:117
#, python-brace-format
msgid "Prefix: {prefix}"
msgstr "Prefix: {prefix}"

#: src/grapejuice/windows/main_window.py:84
msgid "Checking for a newer version of Grapejuice"
msgstr "Buscando una nueva versión de Grapejuice"

#: src/grapejuice/windows/main_window.py:94
msgid "This version of Grapejuice is out of date."
msgstr "Esta versión de Grapejuice está desactualizada."

#: src/grapejuice/windows/main_window.py:99
msgid "This version of Grapejuice is from the future"
msgstr "Esta versión de Grapejuice es del futuro"

#: src/grapejuice/windows/main_window.py:103
msgid "Grapejuice is up to date"
msgstr "Grapejuice está actualizado"

#: src/grapejuice/windows/main_window.py:272
msgid "This installation of Grapejuice does not support updating itself."
msgstr "Esta instalación de Grapejuice no puede actualizarse sola."

#: src/grapejuice/windows/main_window.py:276
msgid ""
"Grapejuice will now update and will re-open after the process has finished.\n"
"If Grapejuice does not re-open, you might have to redo your source install."
msgstr ""
"Grapejuice ahora se actualizará. Se abrirá después de la actualización.\n"
"Si Grapejuice no se abre, reinstale Grapejuice."

#: src/grapejuice/windows/main_window.py:380
msgid "Delete Wineprefix"
msgstr "Eliminar Wineprefix"

#: src/grapejuice/windows/main_window.py:381
#, python-brace-format
msgid "Do you really want to delete the Wineprefix '{prefix}'?"
msgstr ""
"¿Está seguro de que quiere eliminar el Wineprefix «{prefix}»? [s/N] "

#: src/grapejuice/windows/main_window.py:466
#, python-brace-format
msgid "New Wineprefix - {n}"
msgstr "Wineprefix Nuevo - {n}"

#: src/grapejuice/windows/settings_window.py:44
msgid "Uninstall Grapejuice"
msgstr "Desinstalar Grapejuice"

#: src/grapejuice/windows/settings_window.py:44
msgid "Are you sure that you want to uninstall Grapejuice?"
msgstr "¿Está seguro de que quiere desinstalar Grapejuice?"

#: src/grapejuice/windows/settings_window.py:50
msgid "Remove Wineprefixes?"
msgstr "¿Eliminar Wineprefixes?"

#: src/grapejuice/windows/settings_window.py:51
msgid ""
"Do you want to remove all the Wineprefixes Grapejuice has created? Doing "
"this will permanently remove all Roblox program files from this machine. If "
"you have stored Roblox experiences or models inside of a Wineprefix, these "
"will be deleted as well. "
msgstr ""
"¿Quiere eliminar todos los Wineprefixes Grapejuice ha creado? Esto borrara "
"todos los archivos de Roblox en este ordenador. Si has almacenado experiencias "
"de Roblox o modelos dentro de un Wineprefix, estos también se eliminaran."

#: src/grapejuice/windows/settings_window.py:59
msgid ""
"Grapejuice will now uninstall itself and will close when the process is "
"finished."
msgstr ""
"Grapejuice ahora se va a desinstalar, el programa se va a cerrar cuando el "
"proceso termine."

#: src/grapejuice/windows/settings_window.py:74
msgid "Installation Actions"
msgstr "Acciones de Instalación"

#: src/grapejuice/windows/settings_window.py:75
msgid "Manage your Grapejuice installation"
msgstr "Gestionar su instalación de Grapejuice"

#: src/grapejuice/windows/settings_window.py:79
#: src/grapejuice/windows/settings_window.py:83
msgid "Reinstall"
msgstr "Reinstalar"

#: src/grapejuice/windows/settings_window.py:80
msgid "Performing this action will reinstall Grapejuice."
msgstr "Realizar esta acción reinstalará Grapejuice."

#: src/grapejuice/windows/settings_window.py:89
#: src/grapejuice/windows/settings_window.py:93
msgid "Uninstall"
msgstr "Desinstalar"

#: src/grapejuice/windows/settings_window.py:90
msgid "Completely remove Grapejuice from your system!"
msgstr "¡Eliminar Grapejuice del sistema!"

#: src/grapejuice/windows/settings_window.py:106
msgid "Show Fast Flag warning"
msgstr "Mostrar la advertencia sobre Fast Flags"

#: src/grapejuice/windows/settings_window.py:107
msgid "Should Grapejuice warn you when opening the Fast Flag Editor?"
msgstr "¿Debería Grapejuice advertirle al abrir el Editor de Fast Flags?"

#: src/grapejuice/windows/settings_window.py:113
msgid ""
"This is an advanced debugging feature only meant for people who work on Wine "
"itself."
msgstr ""
"Esta es una función de depuración destinada a las personas que trabajan en "
"Wine."

#: src/grapejuice/windows/settings_window.py:119
msgid "Ignore Wine version"
msgstr "Ignorar la versión de Wine"

#: src/grapejuice/windows/settings_window.py:124
msgid "Try profiling hardware"
msgstr "Intentar perfilar hardware"

#: src/grapejuice/windows/settings_window.py:125
msgid ""
"When this setting is enabled, Grapejuice will try profiling your hardware on "
"startup. This profiling step only happens when the hardware profile is not "
"set or when the current hardware does not match the previously profiled "
"hardware. This setting is automatically disabled if hardware profiling fails."
msgstr ""
"Cuando esta configuración está habilitada, Grapejuice intentará perfilar el "
"hardware al iniciarse. Este perfilamiento solo ocurre cuando el perfil del "
"hardware no esta puesto o cuando el hardware no concuerda con lo perfilado "
"anteriormente. Esta opción es desactivada automáticamente cuando el "
"perfilamiento falla."

#: src/grapejuice/windows/settings_window.py:137
msgid "Disable self-updater"
msgstr "Desactivar autoactualizador"

#: src/grapejuice/windows/settings_window.py:143
msgid "Release Channel"
msgstr "Release Channel"

#: src/grapejuice/windows/settings_window.py:144
msgid ""
"Determines from which branch Grapejuice should be updated. This only works "
"for source installs."
msgstr ""
"Determina desde qué rama Grapejuice debe actualizarse. Esto solo funciona si "
"Grapejuice fue instalado desde el código fuente."

#: src/grapejuice/windows/settings_window.py:150
msgid "General"
msgstr "General"

#: src/grapejuice/windows/settings_window.py:151
msgid "These are general Grapejuice settings"
msgstr "Estos son ajustes generales de Grapejuice"

#: src/grapejuice_common/assets/glade/about.glade:11
msgid "BrinkerVII"
msgstr "BrinkerVII"

#: src/grapejuice_common/assets/glade/about.glade:12
msgid "A GUI and wrapper to manage Roblox on Linux running under Wine."
msgstr "Una interfaz para gestionar Roblox en Linux usando Wine."

#: src/grapejuice_common/assets/glade/about.glade:13
msgid ""
"This program comes with absolutely no warranty.\n"
"See the <a href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU General "
"Public Licence, version 3 or later</a> for details."
msgstr ""
"Este programa viene sin absolutamente ninguna garantía.\n"
"Consulte la <a href=\"https://www.gnu.org/licenses/gpl-3.0.html\">Licencia "
"Pública General de GNU, versión 3 o posterior</a> para obtener más detalles."

#: src/grapejuice_common/assets/glade/exception_viewer.glade:33
msgid "Grapejuice has run into some problems! Please check the details below."
msgstr ""
"¡Grapejuice se ha topado con un problema! Por favor, revisa los detalles "
"debajo."

#: src/grapejuice_common/assets/glade/exception_viewer.glade:48
msgid "I am supposed to be the title of the problem"
msgstr "Se supone que soy el título del problema."

#: src/grapejuice_common/assets/glade/exception_viewer.glade:65
msgid "I am supposed to be a semi-human-readable description of the problem."
msgstr "Se supone que soy una descripcíon del problema semi-legible."

#: src/grapejuice_common/assets/glade/exception_viewer.glade:80
msgid ""
"Following is the technical mumbo jumbo associated with the error. If it "
"isn't very useful to you, it might be for the developers or support "
"volunteers! Click the export button in the top left in order to save the "
"technical information to a file."
msgstr ""
"Lo siguiente son detalles técnicos asociados con el error. ¡Si los detalles "
"no son muy utiles para ti, pueden ser para los desarolladores o voluntarios! "
"Presiona el botón exportar a la izquierda para guardar los detalles técnicos "
"a un archivo."

#: src/grapejuice_common/assets/glade/exception_viewer.glade:140
msgid "Oh no! Grapejuice ran into a problem!"
msgstr "¡Oh no! ¡Grapejuice se ha topado con un problema!"

#: src/grapejuice_common/assets/glade/exception_viewer.glade:144
msgid "Export"
msgstr "Exportar"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:33
msgid "Fast flag name"
msgstr "Nombre de Fast Flag"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:144
msgid "Reset"
msgstr "Restablecer"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:184
msgid "Reload flags from settings"
msgstr "Volver a cargar los Flags desde la configuración"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:198
msgid "Set all flags to Roblox' defaults"
msgstr "Establecer los Flags predeterminados de Roblox."

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:223
msgid "Delete flag files"
msgstr "Eliminar Fast Flags"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:324
msgid "Fast Flag Editor"
msgstr "Editor de Fast Flags"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:337
msgid "Save flags"
msgstr "Guardar Fast Flags"

#: src/grapejuice_common/assets/glade/fast_flag_editor.glade:354
msgid "Undo changes"
msgstr "Revertir cambios"

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:92
msgid "If you proceed, the fast flag editor will open."
msgstr "Si continua, el editor de fast flags se abrirá."

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:106
msgid ""
"Fast flags are a way of toggling and changing the behaviour of unreleased "
"Roblox features.\n"
"Changing any fast flags, may lead to data corruption and unstable behaviour "
"of Roblox.\n"
"<b>You should only use this tool if you know what you are doing!</b>"
msgstr ""
"Los Fast flags son un método para cambiar el comportamiento de características "
"inéditas.\n"
"Cambiar cualquier fast flags puede causar corrupción de datos y "
"comportamiento inestable de Roblox.\n"
"<b>¡Solo debe usar esta herramienta si sabe lo que está haciendo!</b>"

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:120
msgid "Read more on the Roblox Developer forum"
msgstr "Leer más información en el Roblox Developer Forum"

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:159
msgid "Show this dialog before opening the Fast Flag Editor"
msgstr "Mostrar esta ventana antes de abrir el Editor de Fast Flags"

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:178
msgid "Open editor"
msgstr "Abrir el editor"

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:204
msgid "Do not open"
msgstr "No abrir"

#: src/grapejuice_common/assets/glade/fast_flag_warning.glade:300
msgid "WARNING!"
msgstr "¡ADVERTENCIA!"

#: src/grapejuice_common/assets/glade/grapejuice.glade:52
msgid "Grapejuice is running 0 tasks in the background"
msgstr "Grapejuice está ejecutando 0 trabajos en el fondo"

#: src/grapejuice_common/assets/glade/grapejuice.glade:81
#: src/grapejuice_common/assets/glade/grapejuice.glade:374
msgid "Off"
msgstr "Apagado"

#: src/grapejuice_common/assets/glade/grapejuice.glade:84
msgid "1.91"
msgstr "1.91"

#: src/grapejuice_common/assets/glade/grapejuice.glade:87
msgid "1.7"
msgstr "1.7"

#: src/grapejuice_common/assets/glade/grapejuice.glade:90
msgid "1.69"
msgstr "1.69"

#: src/grapejuice_common/assets/glade/grapejuice.glade:111
#: src/grapejuice_common/assets/glade/settings.glade:58
msgid "Settings"
msgstr "Configuración"

#: src/grapejuice_common/assets/glade/grapejuice.glade:125
msgid "Open logs directory"
msgstr "Abrir el directorio de registros"

#: src/grapejuice_common/assets/glade/grapejuice.glade:139
msgid "Check for updates"
msgstr "Haz clic para buscar actualizaciones"

#: src/grapejuice_common/assets/glade/grapejuice.glade:163
msgid "About Grapejuice"
msgstr "Sobre Grapejuice"

#: src/grapejuice_common/assets/glade/grapejuice.glade:176
#: src/grapejuice_common/assets/glade/grapejuice.glade:338
msgid "Documentation"
msgstr "Documentación"

#: src/grapejuice_common/assets/glade/grapejuice.glade:261
msgid "Grapejuice"
msgstr "Grapejuice"

#: src/grapejuice_common/assets/glade/grapejuice.glade:298
msgid "Sign in to Studio"
msgstr "Iniciar sesión a Roblox Studio"

#: src/grapejuice_common/assets/glade/grapejuice.glade:318
msgid "Open Roblox Studio"
msgstr "Abrir Roblox Studio"

#: src/grapejuice_common/assets/glade/grapejuice.glade:377
msgid "PRIME"
msgstr "PRIME"

#: src/grapejuice_common/assets/glade/grapejuice.glade:380
msgid "NVIDIA PRIME"
msgstr "NVIDIA PRIME"

#: src/grapejuice_common/assets/glade/grapejuice.glade:383
msgid "Optimus"
msgstr "Optimus"

#: src/grapejuice_common/assets/glade/grapejuice.glade:394
msgid "Let Roblox decide"
msgstr "Dejar que Roblox decida"

#: src/grapejuice_common/assets/glade/grapejuice.glade:397
msgid "Vulkan"
msgstr "Vulkan"

#: src/grapejuice_common/assets/glade/grapejuice.glade:400
msgid "OpenGL"
msgstr "OpenGL"

#: src/grapejuice_common/assets/glade/grapejuice.glade:403
msgid "DirectX 11"
msgstr "DirectX 11"

#: src/grapejuice_common/assets/glade/grapejuice.glade:423
msgid "A new version of Grapejuice is available"
msgstr "Una nueva versión de Grapejuice está disponible"

#: src/grapejuice_common/assets/glade/grapejuice.glade:435
msgid "0.0.0 -> 4.0.0"
msgstr "0.0.0 -> 4.0.0"

#: src/grapejuice_common/assets/glade/grapejuice.glade:445
msgid "Update now"
msgstr "Actualizar ahora"

#: src/grapejuice_common/assets/glade/grapejuice.glade:592
msgid "Grapejuice has run into some trouble. Click to see details."
msgstr ""
"Grapejuice se ha topado con un problema. Haz clic para ver los detalles."

#: src/grapejuice_common/assets/glade/grapejuice.glade:643
msgid "page0"
msgstr "página0"

#: src/grapejuice_common/assets/glade/grapejuice.glade:651
msgid "A"
msgstr "A"

#: src/grapejuice_common/assets/glade/grapejuice.glade:655
msgid "page1"
msgstr "página1"

#: src/grapejuice_common/assets/glade/grapejuice.glade:664
msgid "B"
msgstr "B"

#: src/grapejuice_common/assets/glade/grapejuice.glade:668
msgid "page2"
msgstr "página2"

#: src/grapejuice_common/assets/glade/grapejuice.glade:710
msgid "Configuration"
msgstr "Configuración"

#: src/grapejuice_common/assets/glade/grapejuice.glade:724
msgid "Explorer"
msgstr "Explorador"

#: src/grapejuice_common/assets/glade/grapejuice.glade:738
msgid "Registry Editor"
msgstr "Editor del Registro"

#: src/grapejuice_common/assets/glade/grapejuice.glade:752
msgid "Task Manager"
msgstr "Administrador de Tareas"

#: src/grapejuice_common/assets/glade/grapejuice.glade:766
msgid "Winetricks"
msgstr "Winetricks"

#: src/grapejuice_common/assets/glade/grapejuice.glade:791
msgid "Kill Wineserver"
msgstr "Terminar el wineserver"

#: src/grapejuice_common/assets/glade/grapejuice.glade:855
msgid "Create"
msgstr "Creár"

#: src/grapejuice_common/assets/glade/grapejuice.glade:855
msgid "Initialize"
msgstr "Inicializar"

#: src/grapejuice_common/assets/glade/grapejuice.glade:868
msgid "Delete"
msgstr "Eliminar"

#: src/grapejuice_common/assets/glade/grapejuice.glade:881
msgid "Save Changes"
msgstr "Guardar cambios"

#: src/grapejuice_common/assets/glade/grapejuice.glade:919
msgid "Install Roblox"
msgstr "Instalar Roblox"

#: src/grapejuice_common/assets/glade/grapejuice.glade:935
msgid "Open Drive C"
msgstr "Abrir Disco C"

#: src/grapejuice_common/assets/glade/grapejuice.glade:951
msgid "Edit FFlags"
msgstr "Editar Fast Flags"

#: src/grapejuice_common/assets/glade/grapejuice.glade:1016
msgid "Wine Apps"
msgstr "Aplicaciones de Wine"

#: src/grapejuice_common/assets/glade/grapejuice_components.glade:41
msgid "0/Infinity"
msgstr "0/Infinito"

#: src/grapejuice_common/assets/glade/settings.glade:22
msgid "Open settings file"
msgstr "Abrir el archivo de configuración."

#: src/grapejuice_common/gtk/gtk_paginator.py:40
msgid "No model"
msgstr "Sin modelo"

#: src/grapejuice_common/gtk/yes_no_dialog.py:3
msgid "Untitled Dialog"
msgstr "Diálogo sin título"

#: src/grapejuice_common/gtk/yes_no_dialog.py:3
msgid "This is a message"
msgstr "Esto es un mensaje"
